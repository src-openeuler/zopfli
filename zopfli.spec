Name:           zopfli
Version:        1.0.3
Release:        4
Summary:        Compression library programmed in C
License:        Apache-2.0
URL:            https://github.com/google/zopfli
Source0:        https://github.com/google/zopfli/archive/zopfli-%{version}.tar.gz
Patch0:         zopfli-1.0.3-port-to-newer-cmake.patch
BuildRequires:  gcc-c++
BuildRequires:  cmake >= 2.8.11

%description
Zopfli Compression Algorithm is a compression library programmed in C to perform
very good, but slow, deflate or zlib compression.

%package        devel
Requires:       %{name} = %{version}-%{release}
Summary:        Development files for zopfli and zopflipng.

%description    devel
Devolopment files for zopfli and zopflipng.

%prep
%autosetup -n zopfli-zopfli-%{version} -p1

%build
%cmake -DZOPFLI_BUILD_SHARED=ON
%cmake_build

%install
%cmake_install

%files
%license COPYING
%doc CONTRIBUTORS README README.zopflipng
%{_bindir}/%{name}
%{_bindir}/%{name}png
%{_libdir}/lib%{name}.so.*
%{_libdir}/lib%{name}png.so.*

%files devel
%{_libdir}/lib%{name}.so
%{_libdir}/lib%{name}png.so
%{_includedir}/%{name}.h
%{_includedir}/%{name}png_lib.h
%{_libdir}/cmake/Zopfli

%changelog
* Tue Mar 04 2025 Funda Wang <fundawang@yeah.net> - 1.0.3-4
- try build with cmake 4.0

* Fri Feb 28 2025 Funda Wang <fundawang@yeah.net> - 1.0.3-3
- build shared libraries with cmake

* Thu Oct 27 2022 licihua <licihua@huawei.com> -1.0.3-2
- Delete  unused files
 
* Mon Mar 21 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.0.3-1
- Upgrade to version 1.0.3

* Thu Nov 26 2020 lingsheng <lingsheng@huawei.com> - 1.0.1-9
- Fix invalid read outsize allocated memory

* Mon Jan 6 2020 zhujunhao <zhujunhao5@huawei.com> - 1.0.1-8
- Package init
